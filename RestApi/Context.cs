﻿using Microsoft.EntityFrameworkCore;
using RestApi.Models;

namespace RestApi
{
    public class Context: DbContext
    {
        public Context(DbContextOptions<Context> options)
            : base(options)
        {
        }

        public DbSet<PostModel> Posts { get; set; }
        public DbSet<AboutModel> About {get; set;}
    }
}
