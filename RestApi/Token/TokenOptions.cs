﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RestApi.Token
{
    public class TokenOptions
    {
        public string Type => "Bearer";
        public TimeSpan ValidFor => TimeSpan.FromHours(1);

        public string Audience { get; set; }
        public string Issuer { get; set; }
        public string SigningKey { get; set; }
    }
}
