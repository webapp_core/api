﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.EntityFrameworkCore;
using RestApi.Token;
using Swashbuckle.AspNetCore.Swagger;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;


namespace RestApi
{
    public class Startup
    {
        public Startup(IHostingEnvironment env)
        {            
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();
           
            Configuration = builder.Build();

            builder.AddAzureKeyVault(
              $"https://{Configuration.GetValue<string>("Vault")}.vault.azure.net/",
              Configuration.GetValue<string>("ClientId"),
              Configuration.GetValue<string>("ClientSecret"));
            //this is the odd/annoying bit: you have to rebuild after extending builder
            Configuration = builder.Build();
        }

        public IConfigurationRoot Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors();
            var authOptions = Configuration.GetSection(nameof(TokenOptions)).Get<TokenOptions>();
            
            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            })
            .AddJwtBearer(o => {
                o.Audience = authOptions.Audience;
                o.TokenValidationParameters = new TokenValidationParameters {
                    ValidAudience = authOptions.Audience,
                    ValidIssuer = authOptions.Issuer,
                    IssuerSigningKey = authOptions.GetSymmetricSecurityKey()
                };
            });
          
            services.AddDbContext<Context>(o => o.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));
            services.AddOptions();
            services.Configure<TokenOptions>(Configuration.GetSection(nameof(TokenOptions)));
            services.AddSingleton<IConfiguration>(Configuration);

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info { Title = "PostApi", Version = "v1" });
            });

            services.AddMvc();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
             app.UseCors(bu => bu.AllowAnyOrigin()
                .AllowAnyHeader()     
                .WithExposedHeaders("Access-Control-Allow-Origin"));
                
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();
            app.UseSwagger(); 

            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "PostApi");
                c.InjectOnCompleteJavaScript("/swagger-ui/authorization1.js");
            });

            app.UseAuthentication();
            app.UseStaticFiles();            
            app.UseMvc();            
        }
    }
}
