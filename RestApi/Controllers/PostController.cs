using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using RestApi.Models;
using Microsoft.AspNetCore.Authorization;
using Swashbuckle.AspNetCore.SwaggerGen;
using Microsoft.Extensions.Configuration;

namespace RestApi.Controllers
{
    [Route("api/[controller]")]
    [Authorize]
    public class PostController : Controller
    {
        private readonly Context _context;

        public PostController(Context context)
        {
            _context = context;
        }

        [AllowAnonymous]
        [HttpGet("{id}", Name = "GetPost")]
        [SwaggerResponse(200, typeof(PostModel))]
        [SwaggerResponse(404, typeof(NotFoundResult))]
        public IActionResult GetById(int id)
        {
            var res = _context.Posts                
                .FirstOrDefault(p => p.Id == id);

            if (res == null)
            {
                return NotFound();
            }

            return new ObjectResult(res);
        }

        [AllowAnonymous]
        [HttpGet("{page}/{size}")]
        [SwaggerResponse(200, typeof(IEnumerable<PostModel>))]
        [SwaggerResponse(400, typeof(BadRequestResult))]
        public IActionResult GetPage(int page, int size)
        {
            if(page <= 0 || size <= 0)
            {
                return BadRequest();
            }

            page -= 1;
            var res = _context.Posts
                .OrderByDescending(p => p.Id)
                .Skip(page * size)
                .Take(size)
                .ToList();

            if(res == null || !res.Any())
            {
                return BadRequest();
            }

            return new ObjectResult(res);
        }

        [HttpPost]
        [SwaggerResponse(200,typeof(CreatedAtActionResult))]
        [SwaggerResponse(400,  typeof(BadRequestResult))]
        public async Task<ActionResult> Create ([FromBody] PostModel post)
        {
            if(post == null)
            {
                return BadRequest();             
            }

            _context.Add(post);
            await _context.SaveChangesAsync();

            return CreatedAtRoute("GetPost", new { id = post.Id }, post);
        }
    }
}