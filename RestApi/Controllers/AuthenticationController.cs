using System.IdentityModel.Tokens.Jwt;
using RestApi.Token;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.Configuration;

namespace ConductOfCode.Controllers
{
    [Route("api/[controller]")]
    public class AuthenticationController : Controller
    {
        private TokenOptions Options { get; }
        private readonly IConfiguration _config;


        public AuthenticationController(IOptions<TokenOptions> options, IConfiguration config)
        {
            Options = options.Value;
            _config = config;
        }

        [HttpPost]
        public ActionResult Token([FromBody]TokenRequest request)
        {
            if (request == null || request.Username != _config["login"] || request.Password != _config["password"])
            {
                return BadRequest();
            }

            var token = new JwtSecurityToken(
                audience: Options.Audience,
                issuer: Options.Issuer,
                expires: Options.GetExpiration(),
                signingCredentials: Options.GetSigningCredentials());

            return new ObjectResult(new TokenResponse
            {
                token_type = Options.Type,
                access_token = new JwtSecurityTokenHandler().WriteToken(token),
                expires_in = (int)Options.ValidFor.TotalSeconds
            });
        }
    }
}